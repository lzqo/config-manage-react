import React, { useState,useEffect } from 'react';
import type { MenuProps } from 'antd';
import { Button,Menu,Breadcrumb } from 'antd';
import sty from "./style.module.less";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import logoImg from "../assets/img/logo.png";
import {routesLayout} from '../router/routes'
import { Outlet,useNavigate,useLocation  } from 'react-router-dom';
import {RouteLayoutObject} from "../types/router"

type MenuItem = Required<MenuProps>['items'][number];

const smallWid='80px';
const bigWid='200px';

const Sidebar: React.FC = ({collapsed,topMenuKey}) => {//侧边栏

  function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem;
  }
  
  // const sideItems: MenuItem[] = [//侧边菜单数组
  //   getItem('菜单管理', 'menu', <MailOutlined />, [
  //     getItem('菜单配置', '1'),
  //   ]),
  //   getItem('页面管理', 'page', <AppstoreOutlined />, [
  //     getItem('页面配置', '2'),
  //   ]),
  // ];
  //侧边菜单数组
  const list=routesLayout.filter((item) => item.path===topMenuKey)[0].children
  const sideItems: MenuItem[] = list?.map(item=>{
    let obj= getItem(item.title, item.path, item.icon, item.children.map(it=>{
      let c=getItem(it.title, it.path)
      return c
    }))
    return obj
  })

  const navigate = useNavigate();
  const onSelectMenu=({ item, key, keyPath, selectedKeys, domEvent })=>{
    navigate(`/${topMenuKey}/${keyPath[1]}/${keyPath[0]}`);
  }

  return (
    <div className={sty.sidebarBox}>
      <Menu
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}
        items={sideItems}
        style={{height:'100%'}}
        onSelect={onSelectMenu}
      />
    </div>
  );
};

const Top: React.FC = ({collapsed,onToggleCollapsed,topMenuKey,onSetTopMenuKey}:any) => {//顶部栏
  const topItems: MenuProps['items'] = routesLayout.map(item=>{//顶部菜单数组
    const obj={
      label:item.title,
      key:item.path
    }
    return obj
  });

  const onClick: MenuProps['onClick'] = (e) => {
    onSetTopMenuKey(e.key)
  };

  return(
    <div className={sty.topBox}>
        <div className={sty.logoBox} style={{width:collapsed?smallWid:bigWid}}>
          <div></div>
          {/* <img className={sty.logoImg} src={logoImg} alt="" /> */}
          <span onClick={onToggleCollapsed} className="showHand">
          {collapsed ? <MenuUnfoldOutlined className={sty.FoldIcon}/> : <MenuFoldOutlined className={sty.FoldIcon}/>}
          </span>
        </div>
        <Menu onClick={onClick} selectedKeys={[topMenuKey]} mode="horizontal" items={topItems}  className={sty.topMenu} style={{width:collapsed?`calc(100% - ${smallWid}`:`calc(100% - ${bigWid}`}} theme="dark"/>
    </div>
  )
};


const Bread=()=>{
  const { pathname } = useLocation()
  const [breadList, setBreadList] = useState([])
  // let breadList:[]=[]//这种写的话，当breadList改变时，不会引起Breadcrumb组件的重新渲染，Breadcrumb组件里的items一直是一开始的空数组，更新不了视图。

  useEffect(() => {
    getBreadName(pathname)
  }, [pathname])

  const getBreadName=(path:string)=>{
    const pathArr=path.substring(1).split('/')
    const findTitle=(p:string,list:RouteLayoutObject[])=>{
      let title=''
      for(let i=0;i<list.length;i++){
        let it=list[i]
        if(it.path===p){
          title=it.title
          break
        }else{
          title=it.children?.length?findTitle(p,it.children):''
          if(title){
            break
          }
        }
      }
      return title
    }
    const breadArr:any=pathArr.map(path=>{
      return {
        title:findTitle(path,routesLayout)
      }
    })

    setBreadList(breadArr)
    // breadList=breadArr
  }

  return (
    <Breadcrumb items={breadList} style={{paddingBottom:'15px'}}/>
  )
}

const App: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [topMenuKey, setTopMenuKey] = useState('menu');

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };
  
  const onSetTopMenuKey = (key:any) => {
    setTopMenuKey(key);
  };

  return(
    <div className={sty.layout}>
      <Top collapsed={collapsed} onToggleCollapsed={toggleCollapsed} topMenuKey={topMenuKey} onSetTopMenuKey={onSetTopMenuKey}/>
      <div className={sty.sideAndContent}>
        <Sidebar collapsed={collapsed} topMenuKey={topMenuKey}/>
        <div style={{left:collapsed?'-120px':'0'}} className={sty.content}>
         <Bread/>
         <Outlet />
        </div>
      </div>
    </div>
  )
};

export default App;