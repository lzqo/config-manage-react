import { Spin } from 'antd'

export default function Loading() {
  return (
    <div style={{textAlign:'center',padding:'10%'}}>
      <Spin size="large"/>
    </div>
  )
}
