import { RouteObject ,useRoutes } from 'react-router-dom';
// react-router-dom v6 与 v5 差异 https://reactrouter.com/docs/en/v6/upgrading/v5
import { lazy } from 'react';
import Layout from '../layout/index';
import NoMatch from '../views/common/NoMatch/index';
import {routesLayout} from './routes';

const Login = lazy(() => import('../views/common/Login/index'));

const routes:RouteObject[]= [
  {
    path: "/",
    element: <Layout />,
    children:routesLayout
  },
  {
    path: "/login",
    element: <Login />
  },
  {
    path: "*",
    element: <NoMatch />,
  }
];

export function Routes() {

  const element = useRoutes(routes);
  return (
    <>
      {element}
    </>
  );
}