import { lazy } from 'react';
import {RouteLayoutObject} from "../types/router"
import {
  UserOutlined,
  LaptopOutlined,
} from "@ant-design/icons";
const MenuConfig = lazy(() => import('../views/menu/manage/config/index'));
const PageConfig = lazy(() => import('../views/page/manage/config/index'));

export const routesLayout: RouteLayoutObject[] = [
  {
    path:'menu',
    title: "菜单",
    children: [
      {
        path:'menuManage',//为什么前面加menu？要是不加的话，就跟下面page的manage重复了，而ant菜单的key是要唯一的（菜单key用的是这个path），重复的话会出问题，所以就加menu来区分开。
        title:'菜单管理',
        icon: <UserOutlined />,
        children:[
          {
            path:'menuConfig',
            title:'菜单配置',
            element:<MenuConfig />,
          }
        ]
      },
    ]
  },
  {
    path:'page',
    title: "页面",
    children: [
      {
        path:'pageManage',
        title:'页面管理',
        icon: <LaptopOutlined />,
        children:[
          {
            path:'pageConfig',
            title:'页面配置',
            element:<PageConfig />,
          }
        ]
      },
    ]
  },
];