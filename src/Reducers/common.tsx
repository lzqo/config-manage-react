export const counterReducer = (state = 0, action) => {
    switch (action.type) { //通过action.type匹配
        case 'INCREMENT':
            return state + action.payload //取action里面的payload
        case 'DECREMENT':
            return state - 1
        default : 
            return state;
    }
}
export const loggedReducer = (state = false, action) => {
    switch (action.type) {
        case 'isLoggedIn':
            return !state;
        default:
            return state;
    }
}
