import {counterReducer,loggedReducer} from './common';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    counter : counterReducer, //counter是任意命名的，counterReducer是'./common'中的reducer
    isLogged : loggedReducer
})

export default allReducers;