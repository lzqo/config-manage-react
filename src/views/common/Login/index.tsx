import React from "react";
import { useNavigate  } from "react-router-dom";
import { Form, Input, Button, Checkbox  } from "antd";
import "./style.less";
import logoImg from "../../assets/img/logo.png"

const formItemLayout = {
  wrapperCol: { offset: 10, span: 4 },
};
const Login = () => {

  const navigate=useNavigate();

  const onFinish = (values: any) => {
    console.log("Success:", values);
    navigate('/');
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="login">
      <div className="title">
        <img src={logoImg} alt="" />
        <div>表情包管理后台</div>
      </div>
      <Form
        name="loginForm"
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 4 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="账号"
          name="username"
          rules={[{ required: true, message: "请输入账号!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: "请输入密码!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked" {...formItemLayout}>
          <Checkbox>记住我</Checkbox>
        </Form.Item>

        <Form.Item {...formItemLayout}>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default Login;
