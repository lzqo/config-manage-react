import React,{useImperativeHandle, useState } from 'react';
import { Space, Table, Tag ,Input,Button,Modal  } from 'antd';
import type { TableProps } from 'antd';
import sty from "./style.module.less";
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from '../../../../Actions';
import { apiGetData } from '../../../../request/api'

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
  children?: DataType[];
}

const columns: TableProps<DataType>['columns'] = [
  {
    title: '菜单名称',
    dataIndex: 'name',//相当于vue的prop
    key: 'name',
    render: (text) => <a>{text}</a>,
  },
  {
    title: '图标',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '菜单链接',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: (_, { tags }) => (
      <>
        {tags.map((tag) => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: '操作',
    key: 'action',
    render: (_, record) => (
      <Space size="middle">
        <a>Invite {record.name}</a>
        <Button type="primary" ghost>编辑</Button>
        <Button danger>删除</Button>
      </Space>
    ),
  },
];

const data: DataType[] = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
    children:[
      {
        key: '21',
        name: 'Jim Green11',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
        children:[
          {
            key: '211',
            name: 'John Brown211',
            age: 32,
            address: 'New York No. 1 Lake Park',
            tags: ['nice', 'developer'],
          }
        ]
      },
      {
        key: '22',
        name: 'Jim Green22',
        age: 42,
        address: 'London No. 1 Lake Park',
        tags: ['loser'],
      },
    ]
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sydney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];

interface CounterProp {
  counter: number;
}
const Counter=({counter}:CounterProp)=>{
  return (
    <span>counter：{counter}</span>
  )
}

const AddBox=()=>{
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Button type="primary" className={sty.mr15} onClick={showModal}>新增</Button>
      <Modal title="新增" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </>
  )
}

export default function Page() {
  const counter = useSelector(state => state.counter) //provider中拿到的全局状态
  const isLogged = useSelector(state => state.isLogged)
  const dispatch = useDispatch();

  const onSearch=()=>{
    let params = { type: 2, author: '哗啦啦',arr:[1,2,3] }
    apiGetData(params).then((res:object) => { 
      console.log('233333',res)
    })
  }

  return (
    <>
      <div className={`${sty.searchBar} ${sty.disFlex}`}>
         <div className={sty.disFlex+' '+sty.mr15}>
           <span className={sty.inputTitle}>菜单名称</span>
           <Input/>
         </div>
         <div className={sty.disFlex+' '+sty.mr15}>
           <span className={sty.inputTitle}>菜单名称</span>
           <Input/>
         </div>
         <div>
           <Button type="primary" className={sty.mr15} onClick={onSearch}>搜索</Button>
           <AddBox/>
           <Button type="primary" onClick={() => { dispatch(increment(5)) }}>加数字</Button>
           <Counter counter={counter}/>
         </div>
      </div>
      <Table columns={columns} dataSource={data} />
    </>
  );
}