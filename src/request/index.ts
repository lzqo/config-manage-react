import axios from 'axios';
//引入qs模块，用来序列化post类型的数据
import QS from 'qs';
import { message } from 'antd'

export const baseURL = import.meta.env.VITE_BASE_URL

//设置axios基础路径
const service = axios.create({
  baseURL: baseURL,
  // withCredentials: true, // 跨域请求时发送Cookie  当配置了withCredentials = true时，必须在后端增加 response 头信息Access-Control-Allow-Origin，且必须指定域名，而不能指定为*
  // timeout: 60000, // 请求超时
})

// 请求拦截器
service.interceptors.request.use((config:any) => { 
  // 每次发送请求之前看看本地存储中是否存在token
  // 如果存在，则统一在http请求加上token，这样后台根据token判断用户登录情况
  // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断 
  const token = window.localStorage.getItem('token');
  //在每次的请求中添加token
  config.data = Object.assign({}, config.data, {
    token: token,
  })
  //设置请求头
  config.headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
  }
  //序列化请求参数，不然post请求参数后台接收不正常
  config.data = QS.stringify(config.data)
  return config
}, (error:any) => { 
    return error;
})

// 响应拦截器
service.interceptors.response.use((response:any) => {
  //根据返回不同的状态码做不同的事情
  if (response.code) {
    switch (response.code) {
      case 200:
        return response.data;
      case 401:
        //未登录处理方法
        break;
      case 403:
        //token过期处理方法
        break;
      default:
        message.error(response.data.msg)
    }
  } else { 
    return response;
  }
})
export default service