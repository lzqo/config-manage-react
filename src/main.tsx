import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { createStore } from 'redux';
import {Provider} from 'react-redux';
import allReducers from './Reducers/index';

const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() //Redux DevTools插件(chrome)，便于可视化 
);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
   <Provider store={store}> 
     <App />
   </Provider>
  </React.StrictMode>,
)
